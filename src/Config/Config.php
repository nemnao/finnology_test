<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Config;

/**
 * Class Config
 * @package Finnology\Config
 */
class Config
{
    const DB_METHOD_MYSQL = 1;
    const DB_METHOD_ELASTICSEARCH = 2;

    /**
     * Supported methods:
     * DB_METHOD_ELASTICSEARCH - returns from ElasticSearch
     * DB_METHOD_MYSQL - return from MySQL
     */
    const DB = self::DB_METHOD_ELASTICSEARCH;

    const CACHE_FILE_DIRECTORY = 'cache/';
    const CACHE_REDIS_CONNECT = 'tcp://127.0.0.1:6379?database=10';
}