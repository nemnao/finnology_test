<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Driver\Cache;

use Finnology\Driver\Cache\AbstractAdapter;
use Finnology\Config\Config;
use Predis\Client;

class RedisAdapter extends AbstractAdapter
{
    private $client;
    
    public function __construct($namespace = '')
    {
        parent::__construct($namespace);

        $this->client = new Client(Config::CACHE_REDIS_CONNECT);
    }

    /**
     * Fetches several cache items.
     * @param array $ids
     * @return mixed
     */
    protected function doFetch(array $ids)
    {
        $return = array();
        foreach ($this->client->mget($ids) as $id => $value){
             $return[$id] = $value ? parent::unserialize($value) : false;
        }

        return $return;
    }

    /**
     * Persists several cache items immediately.
     * @param array $values
     * @param $ttl
     * @return mixed
     */
    protected function doSave(array $values, $ttl)
    {
        $ok = true;

        foreach ($values as $id => $value) {
            $ok = $this->client->set($id, serialize($value));
            if ($ttl) {
                $this->client->expire($id, $ttl);
            }
        }

        return $ok;
    }

    /**
     * Increments the number stored at key by one.
     * @param string $id
     * @return int
     */
    protected function doIncrement($id)
    {
        return $this->client->incr($id);
    }
}