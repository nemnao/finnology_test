<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Driver\Cache;

use Exception;

/**
 * Class AbstractAdapter
 * @package Finnology\Driver\Cache
 */
abstract class AbstractAdapter
{
    private $namespace;

    protected function __construct($namespace = '')
    {
        $this->namespace = $namespace;
    }

    /**
     * Fetches several cache items.
     * @param array $ids
     * @return mixed
     */
    abstract protected function doFetch(array $ids);

    /**
     * Persists several cache items immediately.
     * @param array $values
     * @param $ttl
     * @return mixed
     */
    abstract protected function doSave(array $values, $ttl);

    /**
     * Increments the number stored at key by one.
     * @param string $id
     * @return int
     */
    abstract protected function doIncrement($id);

    /**
     * {@inheritdoc}
     */
    public function getItem($key){
        if(!$key){
            return false;
        }

        $id = $this->getId($key);

        $value = null;

        try {
            foreach ($this->doFetch(array($id)) as $value) {
                //set isHit;
            }
        } catch (Exception $e) {
            return false;
        }

        return $value;

    }

    /**
     * {@inheritdoc}
     */
    public function setItem($key, $item, $ttl = 0){
        if(!$key){
            return false;
        }

        $id = $this->getId($key);

        try{
            return $this->doSave(array($id => $item), $ttl);
        } catch (Exception $e){
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setIncrementRequest($key){
        if(!$key){
            return false;
        }

        $id = $this->getId($key);

        return $this->doIncrement($id);

    }

    /**
     * @param $key
     * @return string
     */
    private function getId($key)
    {
        $id = $this->namespace.substr_replace(base64_encode(hash('sha256', $key, true)), ':', -22);

        return $id;
    }

    /**
     * Like the native unserialize() function but throws an exception if anything goes wrong.
     * @param $value
     * @return mixed
     * @throws Exception
     * @throws \ErrorException
     */
    protected static function unserialize($value)
    {
        try {
            if (false !== $value = unserialize($value)) {
                return $value;
            }
            throw new \Exception('Failed to unserialize cached value');
        } catch (\Error $e) {
            throw new \ErrorException($e->getMessage(), $e->getCode(), E_ERROR, $e->getFile(), $e->getLine());
        }
    }


}