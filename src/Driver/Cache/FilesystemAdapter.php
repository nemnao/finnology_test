<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Driver\Cache;

use Exception;
use Finnology\Driver\Cache\AbstractAdapter;
use Finnology\Config\Config;

/**
 * Class FilesystemAdapter
 * @package Finnology\Driver\Cache
 */
class FilesystemAdapter extends AbstractAdapter
{
    private $directory;

    public function __construct($namespace = '')
    {
        parent::__construct($namespace);

        $this->directory = Config::CACHE_FILE_DIRECTORY;
    }

    /**
     * {@inheritdoc}
     */
    protected function doFetch(array $ids)
    {
        $values = array();
        $now = time();

        foreach ($ids as $id) {
            $file = $this->getFile($id);
            if (!file_exists($file) || !$h = @fopen($file, 'rb')) {
                $values[$id] = 0;
                continue;
            }
            if ($now >= (int) $expiresAt = fgets($h)) {
                fclose($h);
                if (isset($expiresAt[0])) {
                    @unlink($file);
                }
            } else {
                $i = rawurldecode(rtrim(fgets($h)));
                $value = stream_get_contents($h);
                fclose($h);
                if ($i === $id) {
                    $values[$id] = parent::unserialize($value);
                }
            }
        }

        return $values;
    }

    /**
     * {@inheritdoc}
     */
    protected function doSave(array $values, $ttl)
    {
        $ok = true;
        $expiresAt = time() + ($ttl ?: 31557600);

        foreach ($values as $id => $value) {
            $ok = $this->write($this->getFile($id, true), $expiresAt."\n".rawurlencode($id)."\n".serialize($value), $expiresAt) && $ok;
        }

        if (!$ok && !is_writable($this->directory)) {
            throw new Exception(sprintf('Cache directory is not writable (%s)', $this->directory));
        }

        return $ok;
    }

    private function getFile($id, $mkdir = false)
    {
        $hash = str_replace('/','-', base64_encode(hash('sha256', static::class.$id, true)));
        $dir = $this->directory.strtoupper($hash[0].DIRECTORY_SEPARATOR.$hash[1].DIRECTORY_SEPARATOR);
        if ($mkdir && !file_exists($dir)) {
            @mkdir($dir, 0777, true);
        }
        return $dir.substr($hash, 2, 20);
    }

    private function write($file, $data, $expiresAt = null)
    {
        if (false === @file_put_contents($file, $data)) {
            return false;
        }
        if (null !== $expiresAt) {
            @touch($file, $expiresAt);
        }
        return true;
    }

    /**
     * Increments the number stored at key by one.
     * @param string $id
     * @return int
     */
    protected function doIncrement($id)
    {
        try {
            $values = $this->doFetch(array($id));

            if (is_array($values)){
                foreach ($values as $file => $value) {
                    $increment_value = (int)$value + 1;

                    try{
                        if ($this->doSave(array($id => $increment_value), 0)){
                            return $increment_value;
                        }
                        return false;
                    } catch (Exception $e){
                        return false;
                    }
                }
            }

        } catch (Exception $e) {
            return false;
        }

    }
}