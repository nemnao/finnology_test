<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Driver\MySQL;

/**
 * Class MySQLDriver
 * @package Finnology\Driver\MySQL
 */
class MySQLDriver implements IMySQLDriver
{

    /**
     * {@inheritdoc}
     */
    public function findProduct($id){
        return array('id' => $id, 'name' => 'Test product from MySQL');
    }
}