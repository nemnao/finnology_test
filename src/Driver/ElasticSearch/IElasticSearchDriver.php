<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Driver\ElasticSearch;


interface IElasticSearchDriver
{
    /**
     * @param string $id
     * @return array
     */
    public function findById($id);
}