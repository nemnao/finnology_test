<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Driver\ElasticSearch;

/**
 * Class ElasticSearchDriver
 * @package Finnology\Driver\ElasticSearch
 */
class ElasticSearchDriver implements IElasticSearchDriver
{
    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        return array('id' => $id, 'name' => 'Test product from Elastic');
    }
}