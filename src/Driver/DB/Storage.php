<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Driver\DB;

use Finnology\Driver\MySQL\MySQLDriver;
use Finnology\Driver\ElasticSearch\ElasticSearchDriver;
use Finnology\Config\Config;

/**
 * Class Storage
 * Get data from storages (MySQL or ElasticSearch)
 * @package Finnology\Driver\DB
 */
class Storage
{
    private $dbMethod;
    private $db;

    public function __construct()
    {
        $this->dbMethod = Config::DB;

        if ($this->dbMethod == Config::DB_METHOD_MYSQL){
            $this->db = new MySQLDriver();
        } elseif ($this->dbMethod == Config::DB_METHOD_ELASTICSEARCH){
            $this->db = new ElasticSearchDriver();
        }
    }

    /**
     * Get result from storage
     * @param $id
     * @return array
     */
    public function get($id){

        if ($this->db instanceof MySQLDriver){
            //return from mysql
            return $this->db->findProduct($id);
        }

        if ($this->db instanceof ElasticSearchDriver){
            //return from elasticsearch
            return $this->db->findById($id);
        }

        return false;
    }

    /**
     * Save to MySQL and ElasticSearch
     * @param $value
     * @return bool
     */
    public function set($value){
        //TODO save method

        return true;
    }

}