<?php
/**
 * @author: Oleg Nemna <nemnao@gmail.com>
 */

namespace Finnology\Controller;

use Finnology\Driver\DB\Storage;
use Finnology\Driver\Cache\FilesystemAdapter;
use Finnology\Driver\Cache\RedisAdapter;

class ProductController
{
    /**
     * @param string $id
     * @return string
     */
    public function getDetail($id)
    {
        $key = "p:$id";
        $cache = new FilesystemAdapter("fn:");
        //$cache = new RedisAdapter("fn:");

        $product = $cache->getItem($key);

        if (!$product) {
            $db = new Storage();

            $product = $db->get($id);

            $cache->setItem($key, $product);
        }

        $product['count_show'] = $cache->setIncrementRequest("p:i:$id");

        return json_encode($product);
    }
}