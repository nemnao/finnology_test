# README #

Pro instalaci pouzijte composer.

### Udelano ###

* Class konfigurace 
* Controller s metodou prijimajici id produktu
* Driver ziskavani dat z ElasticSearch a MySQL. V driveru je realizovana jenom logika ziskavani dat. Prepinani zdroju je realizovano v classu konfiguraci.
* Adapter cachovani. Realizovane classes pro cache do souboru a do Redis. Pouziti Redis je preferovano, protoze podle zadani casto mate high-load a pravdepodobne se pouziva nekolik workers s load-balancer. Pri pouziti souboroveho cache udaje na ostatnich workers nebudou konzistentni. Redis ma zabudovany mechanismus replikace, atomic operations. Plus pri pouzivani souboroveho cache budou velke naklady na read/write, co snizi produktivitu systemu.
* Ukladani poctu requestu k produktu je take realizovano pres cache. Podle zadani cache je unlimited. Dobrym resenim bude pouzivani Redis s atomic incrementation. Jestli data jsou potrebny pro oddil marketingu, predpokladam ze bude nutna filtrace podle datumu. Pro tyto ucely je lepsi pouzit Time Series DB. Napriklad influxdb.