<?php
/**
 * Created by PhpStorm.
 * User: oleg
 * Date: 24.01.17
 * Time: 19:03
 */

ini_set("display_errors", "1");
error_reporting(E_ALL);

require __DIR__.'/vendor/autoload.php';

use Finnology\Controller\ProductController;

$product_controller = new ProductController();
$product = $product_controller->getDetail(12);

echo $product;